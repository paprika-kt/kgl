@file:Suppress("unused", "UNUSED_PARAMETER")

package paprika.render

import ca.jsbr.paprika.background.ios.glObject.*
import kotlinx.cinterop.*
import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.FloatArrayBuffer
import paprika.io.buffer.IntArrayBuffer
import paprika.io.buffer.ShortArrayBuffer
import paprika.canvas.sources.ImageSource
import paprika.render.glObject.*
import paprika.render.pglObject.*
import platform.darwin.nil
import platform.gles2.*
import platform.gles3.glFramebufferRenderbuffer
import platform.gles3.glUniform1i

@ExperimentalUnsignedTypes
class IOSRendererContext : RenderingContext {

    private val memScope = MemScope()

    private val TRUE = GL_TRUE.toUByte()
    private val FALSE = GL_FALSE.toUByte()

    override val drawingBufferWidth: Int
        get() = TODO("not implemented")
    override val drawingBufferHeight: Int
        get() = TODO("not implemented")

    override fun getContextAttributes(): PGLContextAttributes? {
        TODO("not implemented")
    }

    override fun isContextLost(): Boolean {
        TODO("not implemented")
    }

    override fun getSupportedExtensions(): Array<String> {
        TODO("not implemented")
    }

    override fun getExtension(name: String): Any {
        TODO("not implemented")
    }

    override fun activeTexture(texture: Int) = glActiveTexture(texture.toUInt())

    override fun attachShader(program: PGLProgram, shader: PGLShader) = glAttachShader(program.id, shader.id)

    override fun bindAttribLocation(program: PGLProgram, index: Int, name: String) =
        glBindAttribLocation(program.id, index.toUInt(), name)

    override fun bindBuffer(target: Int, buffer: PGLBuffer?) = glBindBuffer(target.toUInt(), buffer?.id ?: 0u)

    override fun bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer) =
        glBindFramebuffer(target.toUInt(), framebuffer.id)

    override fun bindRenderbuffer(target: Int, renderbuffer: PGLRenderbuffer) =
        glBindRenderbuffer(target.toUInt(), renderbuffer.id)

    override fun bindTexture(target: Int, texture: PGLTexture) = glBindTexture(target.toUInt(), texture.id)

    override fun blendColor(red: Float, green: Float, blue: Float, alpha: Float) = glBlendColor(red, green, blue, alpha)

    override fun blendEquation(mode: Int) = glBlendEquation(mode.toUInt())

    override fun blendEquationSeparate(modeRGB: Int, modeAlpha: Int) =
        glBlendEquationSeparate(modeRGB.toUInt(), modeAlpha.toUInt())

    override fun blendFunc(sfactor: Int, dfactor: Int) = glBlendFunc(sfactor.toUInt(), dfactor.toUInt())

    override fun blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int) =
        glBlendFuncSeparate(srcRGB.toUInt(), dstRGB.toUInt(), srcAlpha.toUInt(), dstAlpha.toUInt())

    override fun bufferData(target: Int, size: Int, usage: Int) {
        TODO("not implemented")
    }

    override fun bufferData(target: Int, data: PBufferDataSource?, usage: Int) {
        TODO("not implemented")
    }

    override fun bufferData(target: Int, data: FloatArrayBuffer, usage: Int) =
        glBufferData(target.toUInt(), data.byteLength.signExtend(), data.source, usage.toUInt())

    override fun bufferData(target: Int, data: ShortArrayBuffer, usage: Int) =
        glBufferData(target.toUInt(), data.byteLength.signExtend(), data.source, usage.toUInt())

    override fun bufferData(target: Int, data: IntArrayBuffer, usage: Int) =
        glBufferData(target.toUInt(), data.byteLength.signExtend(), data.source, usage.toUInt())

    override fun bufferData(target: Int, data: ByteArrayBuffer, usage: Int) =
        glBufferData(target.toUInt(), data.byteLength.signExtend(), data.source, usage.toUInt())

    override fun bufferSubData(target: Int, offset: Int, data: PBufferDataSource?) {
        TODO("not implemented")
    }

    override fun checkFramebufferStatus(target: Int): Int {
        TODO("not implemented")
    }

    override fun clear(mask: Int) = glClear(mask.toUInt())
    override fun clearColor(red: Float, green: Float, blue: Float, alpha: Float) = glClearColor(red, green, blue, alpha)
    override fun clearDepth(depth: Float) = glClearDepthf(depth)
    override fun clearStencil(s: Int) = glClearStencil(s)
    override fun colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean) = glColorMask(
        if (red) TRUE else FALSE,
        if (green) TRUE else FALSE,
        if (blue) TRUE else FALSE,
        if (alpha) TRUE else FALSE
    )

    override fun compileShader(shader: PGLShader) = glCompileShader(shader.id)

    override fun compressedTexImage2D(
        target: Int,
        level: Int,
        internalformat: Int,
        width: Int,
        height: Int,
        border: Int,
        data: PGLArrayBufferView
    ) {
        TODO("not implemented")
    }

    override fun compressedTexSubImage2D(
        target: Int,
        level: Int,
        xoffset: Int,
        yoffset: Int,
        width: Int,
        height: Int,
        format: Int,
        data: PGLArrayBufferView
    ) {
        TODO("not implemented")
    }

    override fun copyTexImage2D(
        target: Int,
        level: Int,
        internalformat: Int,
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        border: Int
    ) {
        TODO("not implemented")
    }

    override fun copyTexSubImage2D(
        target: Int,
        level: Int,
        xoffset: Int,
        yoffset: Int,
        x: Int,
        y: Int,
        width: Int,
        height: Int
    ) {
        TODO("not implemented")
    }

    override fun createBuffer(): PGLBuffer = localMemScoped {
        val bufferVar: UIntVarOf<UInt> = alloc()
        glGenBuffers(1, bufferVar.ptr)
        IOSBuffer(bufferVar.value)
    }

    override fun createFramebuffer(): WebGLFramebuffer? {
        TODO("not implemented")
    }

    override fun createProgram(): PGLProgram? = IOSProgram(glCreateProgram())

    override fun createRenderbuffer(): PGLRenderbuffer? = TODO()

    override fun createShader(type: Int): PGLShader = IOSShader(glCreateShader(type.toUInt()))

    override fun createTexture(): PGLTexture = localMemScoped {
        val bufferVar: UIntVarOf<UInt> = alloc()
        glGenTextures(1, bufferVar.ptr)
        IOSTexture(bufferVar.value)
    }

    override fun cullFace(mode: Int) = glCullFace(mode.toUInt())

    override fun deleteBuffer(buffer: PGLBuffer) = glDeleteBuffers(1, cValuesOf(buffer.id))

    override fun deleteFramebuffer(framebuffer: WebGLFramebuffer) = glDeleteFramebuffers(1, cValuesOf(framebuffer.id))

    override fun deleteProgram(program: PGLProgram) = glDeleteProgram(program.id)

    override fun deleteRenderbuffer(renderbuffer: PGLRenderbuffer) =
        glDeleteRenderbuffers(1, cValuesOf(renderbuffer.id))

    override fun deleteShader(shader: PGLShader) = glDeleteShader(shader.id)

    override fun deleteTexture(texture: PGLTexture) = glDeleteTextures(1, cValuesOf(texture.id))

    override fun depthFunc(func: Int) = glDepthFunc(func.toUInt())

    override fun depthMask(flag: Boolean) = glDepthMask(if (flag) TRUE else FALSE)

    override fun depthRange(zNear: Float, zFar: Float) = glDepthRangef(zNear, zFar)

    override fun detachShader(program: PGLProgram, shader: PGLShader) = glDetachShader(program.id, shader.id)

    override fun disable(cap: Int) = glDisable(cap.toUInt())

    override fun disableVertexAttribArray(index: Int) = glDisableVertexAttribArray(index.toUInt())

    override fun drawArrays(mode: Int, first: Int, count: Int) = glDrawArrays(mode.toUInt(), first, count)

    override fun drawElements(mode: Int, count: Int, type: Int, offset: Int) {
        glDrawElements(mode.toUInt(), count, type.toUInt(), null)
    }

    override fun enable(cap: Int) = glEnable(cap.toUInt())

    override fun enableVertexAttribArray(index: Int) = glEnableVertexAttribArray(index.toUInt())

    override fun finish() = glFinish()

    override fun flush() = glFlush()

    override fun framebufferRenderbuffer(
        target: Int,
        attachment: Int,
        renderbuffertarget: Int,
        renderbuffer: PGLRenderbuffer
    ) = glFramebufferRenderbuffer(target.toUInt(), attachment.toUInt(), renderbuffertarget.toUInt(), renderbuffer.id)

    override fun framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: PGLTexture, level: Int) =
        glFramebufferTexture2D(target.toUInt(), attachment.toUInt(), textarget.toUInt(), texture.id, level)

    override fun frontFace(mode: Int) = glFrontFace(mode.toUInt())

    override fun generateMipmap(target: Int) = glGenerateMipmap(target.toUInt())

    override fun getActiveAttrib(program: PGLProgram, index: Int): PGLActiveInfo? {
        TODO("not implemented")
    }

    override fun getActiveUniform(program: PGLProgram, index: Int): PGLActiveInfo? {
        TODO("not implemented")
    }

    override fun getAttachedShaders(program: PGLProgram): Array<PGLShader>? {
        TODO("not implemented")
    }

    override fun getAttribLocation(program: PGLProgram, name: String): Int = glGetAttribLocation(program.id, name)

    override fun getBufferParameter(target: Int, pname: Int): Any? = TODO()

    override fun getParameter(pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getError(): Int {
        TODO("not implemented")
    }

    override fun getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getProgramParameter(program: PGLProgram, pname: Int): Any? = localMemScoped {
        val bufferVar = alloc<IntVar>()
        glGetProgramiv(program.id, pname.toUInt(), bufferVar.ptr)
        bufferVar.value
    }

    override fun getProgramInfoLog(program: PGLProgram): String? = localMemScoped {
        val log = allocArray<ByteVar>(8192)
        val length = alloc<IntVar>()
        glGetProgramInfoLog(program.id, 8192, length.ptr, log)
        log.toKString().substring(length.value)
    }

    override fun getRenderbufferParameter(target: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getShaderParameter(shader: PGLShader, pname: Int): Any? = localMemScoped {
        val status = alloc<IntVar>()
        glGetShaderiv(shader.id, pname.toUInt(), status.ptr)
        status.value
    }

    override fun getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): WebGLShaderPrecisionFormat? {
        TODO("not implemented")
    }

    override fun getShaderInfoLog(shader: PGLShader): String? = localMemScoped {
        val log = allocArray<ByteVar>(8192)
        val length = alloc<IntVar>()
        glGetShaderInfoLog(shader.id, 8192, length.ptr, log)
        log.toKString().substring(length.value)
    }

    override fun getShaderSource(shader: PGLShader): String? {
        TODO("not implemented")
    }

    override fun getTexParameter(target: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getUniform(program: PGLProgram, location: PGLUniformLocation): Any? {
        TODO("not implemented")
    }

    override fun getUniformLocation(program: PGLProgram, name: String): PGLUniformLocation =
        IOSUniformLocation(glGetUniformLocation(program.id, name))

    override fun getVertexAttrib(index: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getVertexAttribOffset(index: Int, pname: Int): Int {
        TODO("not implemented")
    }

    override fun hint(target: Int, mode: Int) {
        TODO("not implemented")
    }

    override fun isBuffer(buffer: PGLBuffer): Boolean = glIsBuffer(buffer.id) == TRUE

    override fun isEnabled(cap: Int): Boolean = glIsEnabled(cap.toUInt()) == TRUE

    override fun isFramebuffer(framebuffer: WebGLFramebuffer?): Boolean {
        TODO("not implemented")
    }

    override fun isProgram(program: PGLProgram): Boolean = glIsProgram(program.id) == TRUE

    override fun isRenderbuffer(renderbuffer: PGLRenderbuffer): Boolean = glIsRenderbuffer(renderbuffer.id) == TRUE

    override fun isShader(shader: PGLShader): Boolean = glIsShader(shader.id) == TRUE

    override fun isTexture(texture: PGLTexture): Boolean = glIsTexture(texture.id) == TRUE

    override fun lineWidth(width: Float) = glLineWidth(width)

    override fun linkProgram(program: PGLProgram) = glLinkProgram(program.id)

    override fun pixelStorei(pname: Int, param: Int) = glPixelStorei(pname.toUInt(), param)

    override fun polygonOffset(factor: Float, units: Float) {
        TODO("not implemented")
    }

    override fun readPixels(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        format: Int,
        type: Int,
        pixels: PGLArrayBufferView?
    ) {
        TODO("not implemented")
    }

    override fun renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int) {
        TODO("not implemented")
    }

    override fun sampleCoverage(value: Float, invert: Boolean) {
        TODO("not implemented")
    }

    override fun scissor(x: Int, y: Int, width: Int, height: Int) {
        TODO("not implemented")
    }

    override fun shaderSource(shader: PGLShader, source: String) {
        localMemScoped {
            //            glShaderSource(shader.id, 1, cValuesOf(source.cstr.getPointer(memScope)), null)
            glShaderSource(shader.id, 1, cValuesOf(source.utf8.getPointer(memScope)), null)
        }
    }

    override fun stencilFunc(func: Int, ref: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilMask(mask: Int) {
        TODO("not implemented")
    }

    override fun stencilMaskSeparate(face: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilOp(fail: Int, zfail: Int, zpass: Int) {
        TODO("not implemented")
    }

    override fun stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int) {
        TODO("not implemented")
    }

    override fun texImage2D(
            target: Int,
            level: Int,
            internalformat: Int,
            width: Int,
            height: Int,
            border: Int,
            format: Int,
            type: Int,
            pixels: ByteArrayBuffer
    ) =
        glTexImage2D(
            target.toUInt(),
            level,
            internalformat,
            width,
            height,
            border,
            format.toUInt(),
            type.toUInt(),
            pixels.source
        )

    override fun texImage2D(target: Int, level: Int, internalformat: Int, format: Int, type: Int, source: ImageSource) {
//        if (source is IOSImageSource && source.image != null)
//            return glTexImage2D(target.toUInt(), level, internalformat, source.width, source.height, 0, format.toUInt(), type.toUInt(), source.image)
        glTexImage2D(target.toUInt(), level, internalformat, source.width, source.height, 0, format.toUInt(), type.toUInt(), source.bytes.source)
        // TODO review
//        val c: CArrayPointer<ByteVar>? = null
//        glTexImage2D(
//            target.toUInt(),
//            level,
//            internalformat,
//            source.width,
//            source.height,
//            0,
//            format.toUInt(),
//            type.toUInt(),
//            c
//        )
    }

    override fun texParameterf(target: Int, pname: Int, param: Float) =
        glTexParameterf(target.toUInt(), pname.toUInt(), param)

    override fun texParameteri(target: Int, pname: Int, param: Int) =
        glTexParameteri(target.toUInt(), pname.toUInt(), param)

    override fun texSubImage2D(
        target: Int,
        level: Int,
        xoffset: Int,
        yoffset: Int,
        width: Int,
        height: Int,
        format: Int,
        type: Int,
        pixels: PGLArrayBufferView?
    ) {
        TODO("not implemented")
    }

    override fun texSubImage2D(
        target: Int,
        level: Int,
        xoffset: Int,
        yoffset: Int,
        format: Int,
        type: Int,
        source: ImageSource?
    ) {
        TODO("not implemented")
    }

    override fun uniform1f(location: PGLUniformLocation, x: Float) {
        TODO("not implemented")
    }

    override fun uniform1fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    //TODO review remove
    override fun uniform1fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    //TODO to review
    override fun uniform1i(location: PGLUniformLocation, x: Int) = glUniform1i(location.id.toInt(), x)

    override fun uniform1iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform1iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform2f(location: PGLUniformLocation, x: Float, y: Float) {
        TODO("not implemented")
    }

    override fun uniform2fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform2fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform2i(location: PGLUniformLocation, x: Int, y: Int) {
        TODO("not implemented")
    }

    override fun uniform2iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform2iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform3f(location: PGLUniformLocation, x: Float, y: Float, z: Float) {
        TODO("not implemented")
    }

    override fun uniform3fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform3fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform3i(location: PGLUniformLocation, x: Int, y: Int, z: Int) {
        TODO("not implemented")
    }

    override fun uniform3iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform3iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform4f(location: PGLUniformLocation, x: Float, y: Float, z: Float, w: Float) {
        TODO("not implemented")
    }

    override fun uniform4fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform4fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform4i(location: PGLUniformLocation, x: Int, y: Int, z: Int, w: Int) {
        TODO("not implemented")
    }

    override fun uniform4iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform4iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) {
        TODO("not implemented")
    }

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) {
        TODO("not implemented")
    }

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) =
        glUniformMatrix4fv(location.id, 1, if (transpose) TRUE else FALSE, value.toCValues())

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun useProgram(program: PGLProgram?) = glUseProgram(program?.id ?: 0.toUInt())

    override fun validateProgram(program: PGLProgram) {
        TODO("not implemented")
    }

    override fun vertexAttrib1f(index: Int, x: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib1fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib2f(index: Int, x: Float, y: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib2fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib3f(index: Int, x: Float, y: Float, z: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib3fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib4f(index: Int, x: Float, y: Float, z: Float, w: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib4fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttribPointer(index: Int, size: Int, type: Int, normalized: Boolean, stride: Int, offset: Int) =
        glVertexAttribPointer(
            index.toUInt(),
            size,
            type.toUInt(),
            if (normalized) TRUE else FALSE,
            stride,
            nil
        ) // TODO cValuesOf(offset.toUInt()) not work

    fun vertexAttribPointer3(index: Int, size: Int, type: Int, normalized: Boolean, stride: Int, offset: Int) =
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE.toUByte(), 3 * 4, nil)

    override fun viewport(x: Int, y: Int, width: Int, height: Int) = glViewport(x, y, width, height)

    fun <T> localMemScoped(action: NativePlacement.() -> T): T {
//        return memScope.let(action)
//        return nativeHeap.let(action)
        return memScoped(action)
    }
}

