package paprika.render.pglObject

import paprika.io.buffer.Buffer

typealias PBufferDataSource = Buffer
typealias PGLArrayBufferView = PBufferDataSource
