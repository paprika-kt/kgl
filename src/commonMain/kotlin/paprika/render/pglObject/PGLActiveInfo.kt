package paprika.render.pglObject

interface PGLActiveInfo {
    val size: Int
    val type: Int
    val name: String
}