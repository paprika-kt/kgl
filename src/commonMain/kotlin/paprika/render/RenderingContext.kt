package paprika.render


import paprika.canvas.sources.ImageSource
import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.FloatArrayBuffer
import paprika.io.buffer.IntArrayBuffer
import paprika.io.buffer.ShortArrayBuffer
import paprika.render.pglObject.*

interface RenderingContext  {

    companion object : PGLConstant()

    //    val canvas: HTMLCanvasElement
    val drawingBufferWidth: Int
    val drawingBufferHeight: Int
    fun getContextAttributes(): PGLContextAttributes?
    fun isContextLost(): Boolean
    fun getSupportedExtensions(): Array<String>
    fun getExtension(name: String): Any
    fun activeTexture(texture: Int)
    fun attachShader(program: PGLProgram, shader: PGLShader)
    fun bindAttribLocation(program: PGLProgram, index: Int, name: String)
    fun bindBuffer(target: Int, buffer: PGLBuffer?)
    fun bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer)
    fun bindRenderbuffer(target: Int, renderbuffer: PGLRenderbuffer)
    fun bindTexture(target: Int, texture: PGLTexture)
    fun blendColor(red: Float, green: Float, blue: Float, alpha: Float)
    fun blendEquation(mode: Int)
    fun blendEquationSeparate(modeRGB: Int, modeAlpha: Int)
    fun blendFunc(sfactor: Int, dfactor: Int)
    fun blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int)
    fun bufferData(target: Int, size: Int, usage: Int)

    @Deprecated("to review")
    fun bufferData(target: Int, data: PBufferDataSource?, usage: Int)

    fun bufferData(target: Int, data: FloatArrayBuffer, usage: Int)
    fun bufferData(target: Int, data: ShortArrayBuffer, usage: Int)
    fun bufferData(target: Int, data: IntArrayBuffer, usage: Int)
    fun bufferData(target: Int, data: ByteArrayBuffer, usage: Int)

    @Deprecated("to review")
    fun bufferSubData(target: Int, offset: Int, data: PBufferDataSource?)

    fun checkFramebufferStatus(target: Int): Int
    fun clear(mask: Int)
    fun clearColor(red: Float, green: Float, blue: Float, alpha: Float)
    fun clearDepth(depth: Float)
    fun clearStencil(s: Int)
    fun colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean)
    fun compileShader(shader: PGLShader)
    fun compressedTexImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, data: PGLArrayBufferView)
    fun compressedTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, data: PGLArrayBufferView)
    fun copyTexImage2D(target: Int, level: Int, internalformat: Int, x: Int, y: Int, width: Int, height: Int, border: Int)
    fun copyTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, x: Int, y: Int, width: Int, height: Int)
    fun createBuffer(): PGLBuffer
    fun createFramebuffer(): WebGLFramebuffer?
    fun createProgram(): PGLProgram?
    fun createRenderbuffer(): PGLRenderbuffer?
    fun createShader(type: Int): PGLShader
    fun createTexture(): PGLTexture
    fun cullFace(mode: Int)
    fun deleteBuffer(buffer: PGLBuffer)
    fun deleteFramebuffer(framebuffer: WebGLFramebuffer)
    fun deleteProgram(program: PGLProgram)
    fun deleteRenderbuffer(renderbuffer: PGLRenderbuffer)
    fun deleteShader(shader: PGLShader)
    fun deleteTexture(texture: PGLTexture)
    fun depthFunc(func: Int)
    fun depthMask(flag: Boolean)
    fun depthRange(zNear: Float, zFar: Float)
    fun detachShader(program: PGLProgram, shader: PGLShader)
    fun disable(cap: Int)
    fun disableVertexAttribArray(index: Int)
    fun drawArrays(mode: Int, first: Int, count: Int)
    fun drawElements(mode: Int, count: Int, type: Int, offset: Int)
    fun enable(cap: Int)
    fun enableVertexAttribArray(index: Int)
    fun finish()
    fun flush()
    fun framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: PGLRenderbuffer)
    fun framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: PGLTexture, level: Int)
    fun frontFace(mode: Int)
    fun generateMipmap(target: Int)
    fun getActiveAttrib(program: PGLProgram, index: Int): PGLActiveInfo?
    fun getActiveUniform(program: PGLProgram, index: Int): PGLActiveInfo?
    fun getAttachedShaders(program: PGLProgram): Array<PGLShader>?
    fun getAttribLocation(program: PGLProgram, name: String): Int
    fun getBufferParameter(target: Int, pname: Int): Any?
    fun getParameter(pname: Int): Any?
    fun getError(): Int
    fun getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any?
    fun getProgramParameter(program: PGLProgram, pname: Int): Any?
    fun getProgramInfoLog(program: PGLProgram): String?
    fun getRenderbufferParameter(target: Int, pname: Int): Any?
    fun getShaderParameter(shader: PGLShader, pname: Int): Any? // return PGLShader
    fun getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): WebGLShaderPrecisionFormat?
    fun getShaderInfoLog(shader: PGLShader): String?
    fun getShaderSource(shader: PGLShader): String?
    fun getTexParameter(target: Int, pname: Int): Any?
    fun getUniform(program: PGLProgram, location: PGLUniformLocation): Any?
    fun getUniformLocation(program: PGLProgram, name: String): PGLUniformLocation
    fun getVertexAttrib(index: Int, pname: Int): Any?
    fun getVertexAttribOffset(index: Int, pname: Int): Int
    fun hint(target: Int, mode: Int)
    fun isBuffer(buffer: PGLBuffer): Boolean
    fun isEnabled(cap: Int): Boolean
    fun isFramebuffer(framebuffer: WebGLFramebuffer?): Boolean
    fun isProgram(program: PGLProgram): Boolean
    fun isRenderbuffer(renderbuffer: PGLRenderbuffer): Boolean
    fun isShader(shader: PGLShader): Boolean
    fun isTexture(texture: PGLTexture): Boolean
    fun lineWidth(width: Float)
    fun linkProgram(program: PGLProgram)
    fun pixelStorei(pname: Int, param: Int)
    fun polygonOffset(factor: Float, units: Float)
    fun readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?)
    fun renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int)
    fun sampleCoverage(value: Float, invert: Boolean)
    fun scissor(x: Int, y: Int, width: Int, height: Int)
    fun shaderSource(shader: PGLShader, source: String)
    fun stencilFunc(func: Int, ref: Int, mask: Int)
    fun stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int)
    fun stencilMask(mask: Int)
    fun stencilMaskSeparate(face: Int, mask: Int)
    fun stencilOp(fail: Int, zfail: Int, zpass: Int)
    fun stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int)
    fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: ByteArrayBuffer)
//    fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: UInt8ArrayBuffer): Unit = TODO()
    fun texImage2D(target: Int, level: Int, internalformat: Int, format: Int, type: Int, source: ImageSource)
    fun texParameterf(target: Int, pname: Int, param: Float)
    fun texParameteri(target: Int, pname: Int, param: Int)
    fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?)
    fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, type: Int, source: ImageSource?)
    fun uniform1f(location: PGLUniformLocation, x: Float)
    fun uniform1fv(location: PGLUniformLocation, v: FloatArray)
    @Deprecated("to review")
    fun uniform1fv(location: PGLUniformLocation, v: Array<Float>)

    fun uniform1i(location: PGLUniformLocation, x: Int)
    fun uniform1iv(location: PGLUniformLocation, v: IntArray)
    @Deprecated("to review")
    fun uniform1iv(location: PGLUniformLocation, v: Array<Int>)

    fun uniform2f(location: PGLUniformLocation, x: Float, y: Float)
    fun uniform2fv(location: PGLUniformLocation, v: FloatArray)
    @Deprecated("to review")
    fun uniform2fv(location: PGLUniformLocation, v: Array<Float>)

    fun uniform2i(location: PGLUniformLocation, x: Int, y: Int)
    fun uniform2iv(location: PGLUniformLocation, v: IntArray)
    @Deprecated("to review")
    fun uniform2iv(location: PGLUniformLocation, v: Array<Int>)

    fun uniform3f(location: PGLUniformLocation, x: Float, y: Float, z: Float)
    fun uniform3fv(location: PGLUniformLocation, v: FloatArray)
    @Deprecated("to review")
    fun uniform3fv(location: PGLUniformLocation, v: Array<Float>)

    fun uniform3i(location: PGLUniformLocation, x: Int, y: Int, z: Int)
    fun uniform3iv(location: PGLUniformLocation, v: IntArray)
    @Deprecated("to review")
    fun uniform3iv(location: PGLUniformLocation, v: Array<Int>)

    fun uniform4f(location: PGLUniformLocation, x: Float, y: Float, z: Float, w: Float)
    @Deprecated("to review")
    fun uniform4fv(location: PGLUniformLocation, v: FloatArray)

    @Deprecated("to review")
    fun uniform4fv(location: PGLUniformLocation, v: Array<Float>)

    fun uniform4i(location: PGLUniformLocation, x: Int, y: Int, z: Int, w: Int)
    fun uniform4iv(location: PGLUniformLocation, v: IntArray)
    @Deprecated("to review")
    fun uniform4iv(location: PGLUniformLocation, v: Array<Int>)

    fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray)
    @Deprecated("to review")
    fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>)

    fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray)
    @Deprecated("to review")
    fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>)

    fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray)
    @Deprecated("to review")
    fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>)

    fun useProgram(program: PGLProgram?)
    fun validateProgram(program: PGLProgram)
    fun vertexAttrib1f(index: Int, x: Float)
    fun vertexAttrib1fv(index: Int, values: FloatArray)
    fun vertexAttrib2f(index: Int, x: Float, y: Float)
    fun vertexAttrib2fv(index: Int, values: FloatArray)
    fun vertexAttrib3f(index: Int, x: Float, y: Float, z: Float)
    fun vertexAttrib3fv(index: Int, values: FloatArray)
    fun vertexAttrib4f(index: Int, x: Float, y: Float, z: Float, w: Float)
    fun vertexAttrib4fv(index: Int, values: FloatArray)
    fun vertexAttribPointer(index: Int, size: Int, type: Int, normalized: Boolean, stride: Int, offset: Int)
    fun viewport(x: Int, y: Int, width: Int, height: Int)
}
