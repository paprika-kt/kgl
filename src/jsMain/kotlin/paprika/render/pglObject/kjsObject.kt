package paprika.render.pglObject

import org.khronos.webgl.*


class KJSUniformLocation(val value: WebGLUniformLocation) : PGLUniformLocation
class KJSTexture(val value: WebGLTexture) : PGLTexture
class KJSShader(val value: WebGLShader) : PGLShader
class KJSProgram(val value: WebGLProgram) : PGLProgram
class KJSBuffer(val value: WebGLBuffer) : PGLBuffer
//class TeamVmGLContextAttributes(context: WebGLContextAttributes): PGLContextAttributes, WebGLContextAttributes by context