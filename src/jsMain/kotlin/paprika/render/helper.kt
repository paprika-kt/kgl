package paprika.render

import org.khronos.webgl.*
import paprika.render.pglObject.*

//TODO review
val PGLProgram.value: WebGLProgram
    get() = (this as KJSProgram).value

val PGLTexture.value: WebGLTexture
    get() = (this as KJSTexture).value

val PGLBuffer.value: WebGLBuffer
    get() = (this as KJSBuffer).value

val PGLShader.value: WebGLShader
    get() = (this as KJSShader).value

val PGLUniformLocation.value: WebGLUniformLocation
    get() = (this as KJSUniformLocation).value
